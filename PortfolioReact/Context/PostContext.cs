﻿using Microsoft.EntityFrameworkCore;
using Portfolio.Models;
using PortfolioReact.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.Context
{
    public class PostContext : DbContext
    {
        public PostContext(DbContextOptions<PostContext> options) :
            base(options) {
            Database.Migrate();

            if(postKeys.Count() == 0)
            {
                postKeys.Add(new PostKey() { Active = true, Type = 0xff });
                SaveChanges();
            }
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostKey> postKeys { get; set; }
    }
}
