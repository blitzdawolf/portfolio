﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PortfolioReact.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PortfolioReact.Globals
{
    public static class Read
    {
        public static string ReadBody(HttpRequest request)
        {
            using(var reader = new StreamReader(request.Body))
            {
                return reader.ReadToEnd();
            }
        }

        public static ActionResult GetResponse(PostKey postKey)
        {
            if (postKey == null) return new StatusCodeResult(401);
            if (!postKey.Active) return new StatusCodeResult(401);

            return null;
        }
    }
}
