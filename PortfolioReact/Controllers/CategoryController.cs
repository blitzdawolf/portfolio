﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Portfolio.Context;
using Portfolio.Models;
using PortfolioReact.Globals;
using PortfolioReact.Models;
using PortfolioReact.Requests.Body;
using PortfolioReact.Requests.Header;

namespace PortfolioReact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        PostContext postContext;
        public CategoryController(PostContext postContext)
        {
            this.postContext = postContext;
        }

        public ActionResult<List<Category>> Index()
        {
            return postContext.Categories.ToList();
        }

        [HttpGet(Name = "{name}")]
        public ActionResult<List<Category>> Index(string name)
        {
            try
            {
                return postContext.Categories.Where(x => x.name.Contains(name)).ToList();
            }
            catch (Exception) { }

            return StatusCode(500);
        }

        [HttpPost("create")]
        public ActionResult<string> Create()
        {
            if (!Request.Headers.ContainsKey("key")) return StatusCode(401);
            PostKey postKey = new PostKeyHeader() { keyId = Request.Headers["key"] }.GetPostKey(postContext);

            ActionResult result = Read.GetResponse(postKey);
            if (result != null) return result;

            if (!postKey.hasType(type.Category)) return new StatusCodeResult(401);

            CreateCategoryRequest createCategoryRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateCategoryRequest>(Read.ReadBody(Request));

            Category cat = new Category() { name = createCategoryRequest.Name };
            postContext.Categories.Add(cat);
            postContext.SaveChanges();

            return Ok();
        }
    }
}