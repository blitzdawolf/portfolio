﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Portfolio.Context;
using Portfolio.Models;
using PortfolioReact.Globals;
using PortfolioReact.Models;
using PortfolioReact.Requests.Body;
using PortfolioReact.Requests.Header;

namespace PortfolioReact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        PostContext PostContext;
        public PostController(PostContext postContext)
        {
            PostContext = postContext;
        }

        // GET: api/Post
        [HttpGet]
        public IEnumerable<Post> Get()
        {
            var posts = PostContext.Posts.Where(x => true).ToList();
            return posts;
        }

        [HttpGet("{cat}", Name = "category")]
        public IEnumerable<Post> GetByCategory(string category)
        {
            var posts = PostContext.Posts.Where(x => x.CategoryId.ToString() == category).ToList();
            return posts;
        }

        // GET: api/Post/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Post
        [HttpPost]
        public ActionResult<bool> Post()
        {
            if (!Request.Headers.ContainsKey("key")) return StatusCode(401);
            PostKey postKey = new PostKeyHeader() { keyId = Request.Headers["key"] }.GetPostKey(PostContext);

            ActionResult result = Read.GetResponse(postKey);
            if (result != null) return result;
            if (!postKey.hasType(type.Post)) return new StatusCodeResult(401);

            CreatePostRequest createPostRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<CreatePostRequest>(Read.ReadBody(Request));

            Post p = new Post()
            {
                CategoryId = Guid.Parse(createPostRequest.CategotyId),
                Content = createPostRequest.Content,
                Header = createPostRequest.Header,
                PostKeyId = postKey.Id
            };

            PostContext.Posts.Add(p);
            PostContext.SaveChanges();

            return true;
        }
    }
}
