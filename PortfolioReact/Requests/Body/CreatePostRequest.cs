﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortfolioReact.Requests.Body
{
    public class CreatePostRequest
    {
        public string Header { get; set; }
        public string Content { get; set; }
        public string CategotyId { get; set; }
    }
}
