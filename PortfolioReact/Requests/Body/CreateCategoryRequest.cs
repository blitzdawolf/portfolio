﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PortfolioReact.Requests.Body
{
    [Serializable]
    public class CreateCategoryRequest
    {
        public string Name { get; set; }
    }
}
