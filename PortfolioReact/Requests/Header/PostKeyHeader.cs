﻿using Portfolio.Context;
using PortfolioReact.Models;
using System.Linq;

namespace PortfolioReact.Requests.Header
{
    public class PostKeyHeader
    {
        public string keyId { get; set; }

        public PostKey GetPostKey(PostContext postContext)
        {
            return postContext.postKeys.FirstOrDefault(x => x.Id.ToString() == keyId);
        }
    }
}
