﻿using Newtonsoft.Json;
using Portfolio.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PortfolioReact.Models
{
    public enum type
    {
        Post = 0x01,
        Edit = 0x02,
        Delete = 0x04,
        Category = 0x08,
        CreateKeys = 0x10,
        DeleteKeys = 0x20,
        EditKeys = 0x40,
        e = 0x80
    }

    public class PostKey
    {
        [Key]
        public Guid Id { get; set; }

        public bool Active { get; set; }

        public byte Type { get; set; }

        [InverseProperty("PostKey")]
        public List<Post> Posts { get; set; }

        public bool hasType(type type)
        {
            for (int i = 0; i < 16; i++)
            {
                if ((IsBitSet(Type, i) && IsBitSet((byte)type, i)))
                {
                    return true;
                }
            }

            return false;
        }

        bool IsBitSet(byte b, int pos)
        {
            return (b & (1 << pos)) != 0;
        }
    }
}
