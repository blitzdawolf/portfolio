﻿using PortfolioReact.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portfolio.Models
{
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Header { get; set; }

        public string Content { get; set; }

        public Guid CategoryId { get; set; }

        public Guid PostKeyId { get; set; }

        [ForeignKey("PostKeyId")]
        public PostKey PostKey { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
    }
}
