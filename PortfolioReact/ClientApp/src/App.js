import React, { useState } from 'react';
import {Small} from './components/Small/index';
import {CustomModal} from './components/Modal/index';

class App extends React.Component{
    constructor(props){
        super(props);

        this.state ={
            data: [], loading: true
        };

        this.renderData = this.renderData.bind(this);
        this.OpenPost = this.OpenPost.bind(this);
        this.DisplayPost = this.DisplayPost.bind(this);
        this.ClosePost = this.ClosePost.bind(this);

        fetch('api/post')
        .then(response => response.json())
        .then(data => {
            this.setState({data: data, loading: false});
        })
    }

    renderData(data){
        return(
            <div style={{
                display:'flex',
                'flexWrap': 'wrap'
                }}>
                {data.map(post => <Small Data={post} OnClick={this.OpenPost}/>)}
            </div>
        );
    }

    ClosePost(Data){
        this.setState({currentPost: null});
    }

    OpenPost(Data) {
        this.setState({currentPost: Data});
    }

    DisplayPost(){
        return (<CustomModal OnClose={this.ClosePost} Active={true} Post={this.state.currentPost}/>);
    }

    render(){
        let contents = this.state.loading
        ? <p><em>Loading...</em></p>
        : this.renderData(this.state.data, this);

        let modal = (this.state.currentPost != null)?this.DisplayPost():<div />;

        return (
            <div>
                {modal}
                {contents}
            </div>
        );
    }
}

export default App;
