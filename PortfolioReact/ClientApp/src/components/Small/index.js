import React from 'react';
import './Small.css';
import ReactMarkdown  from 'react-markdown';

function Small({Data, OnClick}) {
    return (
    <button onClick={() => {OnClick(Data)}} className="small-container">
        <h2 style={{textAlign: "center"}}>{Data.header}</h2>
        <ReactMarkdown source={Data.content.substring(0,50)} />
    </button>
    );
}

export { Small };