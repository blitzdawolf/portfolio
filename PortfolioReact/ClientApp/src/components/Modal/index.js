import React from 'react';
import Modal from 'react-modal';
import ReactMarkdown  from 'react-markdown';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        width                 : '80%'
    }
};

class CustomModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: props.Active
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#000';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
    this.props.OnClose();
  }

  render() {

    console.log(this.props);
    const {Post} = this.props;

    return (
      <div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <h2 ref={subtitle => this.subtitle = subtitle}>{Post.header}</h2>
        <ReactMarkdown source={Post.content} escapeHtml={false} />
        </Modal>
      </div>
    );
  }
}

export { CustomModal };