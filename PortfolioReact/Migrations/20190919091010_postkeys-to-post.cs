﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PortfolioReact.Migrations
{
    public partial class postkeystopost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "PostKeyId",
                table: "Posts",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Posts_PostKeyId",
                table: "Posts",
                column: "PostKeyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_postKeys_PostKeyId",
                table: "Posts",
                column: "PostKeyId",
                principalTable: "postKeys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Posts_postKeys_PostKeyId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Posts_PostKeyId",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "PostKeyId",
                table: "Posts");
        }
    }
}
