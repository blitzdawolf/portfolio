# HEADER

## SUB HEADER

## How to use

``` docker
docker run --name some-postgres \
    -e POSTGRES_PASSWORD="test" \
    -e POSTGRES_USER="test" \
    -e POSTGRES_DB="test" \
    -d postgres

docker run -d \
    -p 80:80 -p 443:443 \
    --link some-postgres:db \
    -e VIRTUAL_HOST=portfolio.xenor.duckdns.org \
    -e LETSENCRYPT_HOST=portfolio.xenor.duckdns.org \
    -e LETSENCRYPT_EMAIL = botjes2@hotmail.com \
    -e DBNAME=test \
    -e PORT=5432 \
    -e DATABASE=db \
    -e DBUSE=test \
    -e DBPASS=test \
    blitzdawolf/portfolio:v0.2.0
```

# ENVIROMENTS

## DBNAME
The database host

## PORT
Database port

## DATABASE
database

## DBUSER
Database username

## DBPASS
Database password